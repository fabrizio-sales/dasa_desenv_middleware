﻿// -----------------------------------------------------------------------
// <copyright file="SendXmlToMSMQ.cs" company="IBM Corp.">
// Licensed Materials - Property of IBM
//  ProgIds: 5724-J06 5724-J05 5724-J04 5697-J09 5655-M74 5655-M75 5648-C63, 5655-1BB
// (C) Copyright IBM Corp. 2013 
// </copyright>
// -----------------------------------------------------------------------

using System;
using System.Messaging;
using IBM.Broker.Plugin;

/*namespace DotNetInputSolution
{
    /// <summary>
    /// This class will send a XML file into a queue of MSMQ.
    /// </summary>
    public class SendXmlToMsmq : NBComputeNode
    {
        /// <summary>
        /// MSMQ queue
        /// </summary>
        private MessageQueue msgQ;

        /// <summary>
        /// OnInitialize Method
        /// </summary>
        public override void OnInitialize()
        {
            base.OnInitialize();

            // Get input queue name from message flow user defined properties.
            string inputQueueName = (string)GetUserDefinedProperty("InputMSMQQueueName");

            // Init the message queue.
            this.msgQ = DotNetInputUtility.CreateOrReadPrivateQueue(inputQueueName);            
        }

        /// <summary>
        /// Evaluate Method
        /// </summary>
        /// <param name="inputAssembly">Message Assembly</param>
        public override void Evaluate(NBMessageAssembly inputAssembly)
        {
            NBOutputTerminal outTerminal = OutputTerminal("Out");

            NBMessage inputMessage = inputAssembly.Message;
            NBElement inputRoot = inputMessage.RootElement;

            // Create a new MSMQ message
            Message msmqMessage = new Message();
            CustomerAddress ca = DotNetInputUtility.PopulateCustomerAddress(inputRoot[NBParsers.DFDL.ParserName]);
            msmqMessage.Body = ca;

            // Saving the original MsgId   
            byte[] msgIdBlob = (byte[])inputRoot[NBParsers.NBHeaderParsers.MQMD.ParserName]["MsgId"];
            msmqMessage.CorrelationId = DotNetInputUtility.MQIdToMsmqId(msgIdBlob);
            this.msgQ.Send(msmqMessage);
           
            // Change the following if not propagating message to the 'Out' terminal
            outTerminal.Propagate(inputAssembly);
        }

        /// <summary>
        /// OnDelete Method
        /// </summary>
        public override void OnDelete()
        {
            this.msgQ.Close();
            base.OnDelete();
        }        
    }
}*/
