﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using IBM.Broker.Plugin;

namespace DotNetContarChar
{
    public class CriarNode : NBComputeNode
    {
        public override void Evaluate(NBMessageAssembly inputAssembly)
        {
            //Cria parametro da mensagem de entrada
            NBOutputTerminal outTerminal = OutputTerminal("Out");
            NBMessage inputMessage = inputAssembly.Message;
            NBElement inputRoot = inputMessage.RootElement;

            // Cria parametro da mensagem de saida
            NBMessage outputMessage = new NBMessage();
            NBMessageAssembly outAssembly = new NBMessageAssembly(inputAssembly, outputMessage);
            NBElement outputRoot = outputMessage.RootElement;

            
            //recebe a mensagem de entrada e obtem o valor do byte BLOB
            int tamanho = inputRoot["BLOB"].LastChild.GetByteArray().Length;
            NBElement xmlnscOutputElement = outputRoot.CreateLastChildUsingNewParser("XMLNSC");
            NBElement xmlRoot = xmlnscOutputElement.CreateFirstChild("Output");

            //apresenta a qtd de bytes do dado inputado
            xmlRoot.CreateLastChild("Count").SetValue(tamanho);


            outTerminal.Propagate(outAssembly);

            //throw new NotImplementedException();
        }
    }
}
