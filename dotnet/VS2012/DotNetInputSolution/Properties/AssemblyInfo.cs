﻿using System;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

// General Information about an assembly is controlled through the following 
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.
[assembly: AssemblyTitle(".NETInput Node for MSMQ Sample")]
[assembly: AssemblyDescription("This sample shows how to use the .NETInput node to provide a MSMQ transport input.")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("IBM Corp")]
[assembly: AssemblyProduct("IBM Integration Bus")]
[assembly: AssemblyCopyright("Copyright IBM Corp. 2013")]

// Setting ComVisible to false makes the types in this assembly not visible 
// to COM components.  If you need to access a type in this assembly from 
// COM, set the ComVisible attribute to true on that type.
[assembly: ComVisible(false)]

// The following GUID is for the ID of the typelib if this project is exposed to COM
[assembly: Guid("c37b6550-eca5-40c3-923e-332bbd7af8cd")]

// Version information for an assembly consists of the following four values:
//
//      Major Version
//      Minor Version 
//      Build Number
//      Revision
//
// You can specify all the values or you can default the Build and Revision Numbers 
// by using the '*' as shown below:
// [assembly: AssemblyVersion("1.0.*")]
[assembly: AssemblyVersion("9.0.0.0")]
[assembly: AssemblyFileVersion("9.0.0.0")]

// Set CLS compliance to true
[assembly: CLSCompliant(true)]