﻿// -----------------------------------------------------------------------
// <copyright file="DotNetInputUtility.cs" company="IBM Corp.">
// Licensed Materials - Property of IBM
//  ProgIds: 5724-J06 5724-J05 5724-J04 5697-J09 5655-M74 5655-M75 5648-C63, 5655-1BB
// (C) Copyright IBM Corp. 2013 
// </copyright>
// -----------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Globalization;
using System.Linq;
using System.Messaging;
using IBM.Broker.Plugin;

namespace DotNetInputSolution
{
    /// <summary>
    /// DotNetInputUtility Class
    /// </summary>
    public static class DotNetInputUtility
    {
        /// <summary>
        /// Properties key name for DotNetEventInput in local environment
        /// </summary>
        internal const string DotNetEventInput = "DotNet";

        /// <summary>
        /// Properties key name for Input in local environment
        /// </summary>
        internal const string Input = "Input";

        /// <summary>
        /// Properties key name for CorrelationID in local environment
        /// </summary>
        internal const string CorrelationID = "CorrelationID";

        /// <summary>
        /// Tag name for Customer Addresses
        /// </summary>
        internal const string CustomerAddressTag = "CustomerAddress";

        /// <summary>
        /// Tag name for Addresses
        /// </summary>
        internal const string AddressesTag = "Addresses";

        /// <summary>
        /// Tag name for AddressList
        /// </summary>
        internal const string AddressListTag = "AddressList";

        /// <summary>
        /// Create a new queue OR Read an existed queue based on the input queue name.
        /// </summary>
        /// <param name="queueName">The queue name </param>
        /// <returns>A created Message Queue</returns>
        /*public static MessageQueue CreateOrReadPrivateQueue(string queueName)
        {
            // ***Note***
            // In this sample, the MessageQueue object held in the variable "msgQ" is shared and accessed by all 
            // threads for ease of use, configured by additional instances on the sample flow.
            // Note that in the Microsoft MSDN, this is marked as not being thread-safe. If you want to use code that is
            // based on this sample code in a production message flow, consider this in your implementation. 
            // To avoid a possible threading issue, you could change the code to create one MessageQueue object per thread, 
            // one MessageQueue object per message, or create a pool of MessageQueue objects to retrieve from per message.
            // However, the current use of the MessageQueue APIs in the shipped sample code does not appear to encounter 
            // thread safety issues with additional instances under a sustained message work load.
            MessageQueue msgQ;
            string fullQueueName = ".\\Private$\\" + queueName;
            try
            {
                // Create new or get existed message queue
                if (!MessageQueue.Exists(fullQueueName))
                {
                    msgQ = MessageQueue.Create(fullQueueName);
                    msgQ.SetPermissions("Users", MessageQueueAccessRights.FullControl);
                }
                else
                {
                    msgQ = new MessageQueue(fullQueueName);
                }

                return msgQ;
            }
            catch (InvalidOperationException)
            {
                // Construct an exception to throw
                string errorMsg = "'Microsoft Message Queue (MSMQ) Server' is not installed on this computer.  " +
                                 "Use the 'Control Panel', 'Programs and Features', 'Turn Windows features on or off' to install it.";
                NBUserException exception = new NBUserException(errorMsg, 7531);
                exception.MessageAsFirstInsert = true;
                throw exception;
            }
        }*/

        /// <summary>
        /// Populate CustomerAddress Object based on input message.
        /// </summary>
        /// <param name="inputRoot">NBElement for input Root</param>
        /// <returns>A CustomerAddress object </returns>
        /*public static CustomerAddress PopulateCustomerAddress(NBElement inputRoot)
        {
            CustomerAddress ca = new CustomerAddress();
            NBElement customAddressElement = inputRoot[CustomerAddressTag];
            var attrEles = customAddressElement.Children();
            if (attrEles.Count() >= 0)
            {
                for (int i = 0; i < attrEles.Count(); i++)
                {
                    NBElement attrEle = attrEles.ElementAt(i);
                    switch (attrEle.Name)
                    {
                        case AddressesTag:
                            Addresses address = AddAddressPartIntoAddresses(attrEle, false);
                            ca.AddressList.Add(address);
                            break;
                        default:
                            break;
                    }
                }
            }

            return ca;
        }*/

        /// <summary>
        /// Populate CustomerAddress Object and change the message.
        /// </summary>
        /// <param name="inputRoot">NBElement for input Root</param>
        /// <returns>A CustomerAddress object </returns>
        /*public static CustomerAddress PopulateCustomerAddressFromMsmq(NBElement inputRoot)
        {
            CustomerAddress ca = new CustomerAddress();
            NBElement customAddressElement = inputRoot[CustomerAddressTag];
            var attrEles = customAddressElement.Children();
            if (attrEles.Count() >= 0)
            {
                for (int i = 0; i < attrEles.Count(); i++)
                {
                    NBElement attrEle = attrEles.ElementAt(i);
                    switch (attrEle.Name)
                    {
                        case AddressListTag:
                            Collection<Addresses> addressList = AddAddressListIntoAddresses(attrEle, true);
                            foreach (Addresses address in addressList)
                            {
                                ca.AddressList.Add(address);
                            }

                            break;
                        default:
                            break;
                    }
                }
            }

            return ca;
        }*/

        /// <summary>
        /// Convert an MQ ID to an MSMQ Id.
        /// </summary>
        /// <remarks>
        /// The MQ ID is of the form: '414d5120494239514d47522020202020a377795120006b02' 
        /// The returned ID form is:          '51394249-474d-2052-2020-2020a3777951\40566816'
        /// The MQ ID can be an MQMD 'Message Id' or an MQMD 'Correlation Id'
        /// The MSMQ ID can be an MSMQ Message's 'Id' or an MSMQ Message's 'CorrelationId'
        /// </remarks>
        /// <param name="messageQueueId">The MQ Id. A 'Message Id' or a 'Correlation Id'</param>
        /// <returns>A string version of the MQ Id that can be used with MSMQ as a 'CorrelationId'</returns>
        /*public static string MQIdToMsmqId(byte[] messageQueueId)
        {
            // Skip the first 4 bytes ('AMQ ') and convert the next 16 to a guid
            byte[] uuidPart = new byte[16];
            Array.Copy(messageQueueId, 4, uuidPart, 0, 16);
            Guid guid = new Guid(uuidPart);

            // Turn the guid into a string and add the separator for the sequence number.
            string result = guid.ToString() + "\\";

            // Construct the sequence number from the last 4 bytes (only little endian safe).
            int sequenceNumber = (int)messageQueueId[23];
            sequenceNumber <<= 8;
            sequenceNumber += (int)messageQueueId[22];
            sequenceNumber <<= 8;
            sequenceNumber += (int)messageQueueId[21];
            sequenceNumber <<= 8;
            sequenceNumber += (int)messageQueueId[20];

            // Make the final value
            result += sequenceNumber;
            return result;
        }*/

        /// <summary>
        /// Convert an MSMQ ID an MQ Id.
        /// </summary>
        /// <remarks>
        /// The MSMQ ID is of the form:       '51394249-474d-2052-2020-2020a3777951\40566816'
        /// The returned ID form is: '414d5120 49423951 4d47 5220 2020 2020a3777951 20006b02' (but without the spaces)
        /// The MSMQ ID can be an MSMQ Message's 'Id' or an MSMQ Message's 'CorrelationId'
        /// The MQ ID can be an MQMD 'Message Id' or an MQMD 'Correlation Id'
        /// </remarks>
        /// <param name="msmqId">The MSMQ ID. A Message 'Id' or 'CorrelationId'</param>
        /// <returns>A byte[] version of the MSMQ Id that can be used with MQ as a 'Message Id' or 'Correlation Id'</returns>
        /*public static byte[] MsmqIdToMQId(string msmqId)
        {
            // Create storage for the MQ byte[] which is always 24 bytes long
            byte[] result = new byte[24];

            // First 4 bytes will be 'AMQ '
            result[0] = 0x41;
            result[1] = 0x4D;
            result[2] = 0x51;
            result[3] = 0x20;

            // Convert the first 36 chars up to the '\' to a guid
            Guid guid = new Guid(msmqId.Substring(0, 36));

            // Add in the next 16 bytes from the guid
            Array.Copy(guid.ToByteArray(), 0, result, 4, 16);

            // Finally add in the 4 bytes from the sequence number (only little endian safe).
            int sequenceNumber = Convert.ToInt32(msmqId.Substring(37), CultureInfo.InvariantCulture);
            Array.Copy(BitConverter.GetBytes(sequenceNumber), 0, result, 20, 4);

            return result;
        }*/

        /// <summary>
        /// Read the correlation id from local environment.
        /// </summary>
        /// <param name="localEnvironmentElement">The Local Environment NB Message</param>
        /// <returns>A string of correlation id</returns>
        /*public static string GetCorrelationIdFromLocalEnvironment(NBMessage localEnvironmentElement)
        {
            NBElement element = localEnvironmentElement.RootElement[DotNetEventInput][Input][CorrelationID];
            return element.ValueAsString;
        }*/

        /// <summary>
        /// Populate Addresses List for other using.
        /// </summary>
        /// <param name="addElement">NBElement for Addresses</param>
        /// <param name="changeResult">whether to change the country city and street </param>
        /// <returns>A Addresses object List</returns>
        /*private static Collection<Addresses> AddAddressListIntoAddresses(NBElement addElement, bool changeResult)
        {
            Collection<Addresses> addList = new Collection<Addresses>();
            var attrEles = addElement.Children();
            if (attrEles.Count() >= 0)
            {
                for (int i = 0; i < attrEles.Count(); i++)
                {
                    NBElement attrEle = attrEles.ElementAt(i);
                    switch (attrEle.Name)
                    {
                        case AddressesTag:
                            Addresses address = AddAddressPartIntoAddresses(attrEle, changeResult);
                            addList.Add(address);
                            break;
                        default:
                            break;
                    }
                }
            }

            return addList;
        }*/

        /// <summary>
        /// Add AddressParts into Addresses
        /// </summary>
        /// <param name="addElement">NBElement for Addresses</param>
        /// <param name="changeResult">whether to change the country city and street </param>
        /// <returns>A Addresses object</returns>
        /*private static Addresses AddAddressPartIntoAddresses(NBElement addElement, bool changeResult)
        {
            Addresses address = new Addresses();
            var attrEles = addElement.ElementAt(0).Children();
            if (attrEles.Count() >= 0)
            {
                AddressPart addParts = new AddressPart();
                for (int i = 0; i < attrEles.Count(); i++)
                {
                    NBElement attrEle = attrEles.ElementAt(i);
                    switch (attrEle.Name)
                    {
                        case "HouseNumber":
                            addParts.HouseNumber = attrEle.ValueAsString;
                            break;
                        case "Postcode":
                            addParts.Postcode = attrEle.ValueAsString;
                            break;
                        case "County":
                            if (changeResult)
                            {
                                addParts.County = ".NET County for " + addParts.HouseNumber;
                            }
                            else
                            {
                                addParts.County = attrEle.ValueAsString;
                            }

                            break;
                        case "City":
                            if (changeResult)
                            {
                                addParts.City = ".NET City for " + addParts.HouseNumber;
                            }
                            else
                            {
                                addParts.City = attrEle.ValueAsString;
                            }

                            break;
                        case "Street":
                            if (changeResult)
                            {
                                addParts.Street = ".NET Street for " + addParts.HouseNumber;
                            }
                            else
                            {
                                addParts.Street = attrEle.ValueAsString;
                            }

                            break;
                        case "Country":
                            addParts.Country = "E";
                            break;
                        default:
                            break;
                    }
                }

                address.AddressParts = addParts;
            }

            return address;
        }*/
    }
}
