﻿// -----------------------------------------------------------------------
// <copyright file="PopulateToOutputNode.cs" company="IBM Corp.">
// Licensed Materials - Property of IBM
//  ProgIds: 5724-J06 5724-J05 5724-J04 5697-J09 5655-M74 5655-M75 5648-C63, 5655-1BB
// (C) Copyright IBM Corp. 2013 
// </copyright>
// -----------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using System.Data.OleDb;
using IBM.Broker.Plugin;

namespace DotNetInputSolution
{
    /// <summary>
    /// PopulateToOutputNode Class
    /// </summary>
    public class LerAccessToXml : NBComputeNode
    {
        /// <summary>
        /// Evaluate Method
        /// </summary>
        /// <param name="inputAssembly">Message Assembly</param>
        public override void Evaluate(NBMessageAssembly inputAssembly)
        {

            NBOutputTerminal outTerminal = OutputTerminal("Out");
            NBMessage inputMessage = inputAssembly.Message;
            NBElement inputRoot = inputMessage.RootElement;

            // Create output message
            NBMessage outputMessage = new NBMessage();
            NBMessageAssembly outAssembly = new NBMessageAssembly(inputAssembly, outputMessage);
            NBElement outputRoot = outputMessage.RootElement;

            // Create Properties for output message
            //NBElement propertiesEle = outputRoot.AddFirstChild(inputRoot[MQMDProperties]);

            // Add DFDL Message Type to Properties                
            /*var propEles = propertiesEle.Children();
            for (int i = 0; i < propEles.Count(); i++)
            {
                NBElement propEle = propEles.ElementAt(i);
                switch (propEle.Name)
                {
                    case MQMDPropertiesMessageType:
                        propEle.SetValue("{}:CustomerAddress");
                        break;
                    default:
                        break;
                }
            }
            
            */
             
            // Propagate it to 'Out' terminal
            outTerminal.Propagate(outAssembly);
        }
        public override void OnInitialize()
        {
            base.OnInitialize();
            DataSet dataSet = new DataSet();
            
            using (var conn = new OleDbConnection(@"Provider={Driver do Microsoft Access (*.mdb)};data source=C:\Users\Fabrizio\Documents\Database3.mdb"))
            {
                conn.Open();

                string tableName = @"usuario";
                FillTable(dataSet, conn, tableName);
                conn.Close();
                
            }
            string name = @"C:\Users\Fabrizio\Documents\Database3.mdb";
            dataSet.WriteXml(name.Replace(".mdb", ".xml"));

        }
        private static void FillTable(DataSet dataSet, OleDbConnection conn, string tableName)
        {
            DataTable dataTable = dataSet.Tables.Add(tableName);
            using (OleDbCommand readRows = new OleDbCommand("SELECT * from " + tableName, conn))
            {
                OleDbDataAdapter adapter = new OleDbDataAdapter(readRows);
                adapter.Fill(dataTable);
            }
        }
    }
}
