﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.Odbc;
using System.IO;
using System.Threading.Tasks;

namespace LerAccessXML
{
    class Program
    {
        static void Main(string[] args)
        {
            if (args.Length == 0 || !args[0].EndsWith(".mdb", StringComparison.InvariantCultureIgnoreCase))
            {
                Console.WriteLine("Por favor verifique o parametro informado.\nDeve ser arquivo com extensao.mdb\nInforme o nome da tabela");
                return;
            }
            //Driver={Microsoft Access Driver (*.mdb)};" + @"DBQ=" + args[0]
            //@"dsn=DBTeste;uid=admin;pwd=admin
            DataSet dataSet = new DataSet();
            using (var conn = new OdbcConnection(@"Driver={Microsoft Access Driver (*.mdb)};" + @"DBQ=" + args[0]))
            {
                conn.Open();
                string tableName;
                tableName = args[1];
                if (tableName == "")
                {
                    tableName= @"usuario";
                }
                
                FillTable(dataSet, conn, tableName);
                conn.Close();
            }

            string name = args[0].ToLowerInvariant();
            
            //dataSet.WriteXml(name.Replace(".mdb", ".xml"));
            
            FileStream fs =new FileStream(name.Replace(".mdb", ".xml"), FileMode.CreateNew, FileAccess.Write, FileShare.None); 
            StreamWriter writer = new StreamWriter(fs, Encoding.GetEncoding("ISO-8859-1")); //.UTF8
            dataSet.WriteXml(writer);
            
        }
        private static void FillTable(DataSet dataSet, OdbcConnection conn, string tableName)
        {
            DataTable dataTable = dataSet.Tables.Add(tableName);
            using (OdbcCommand readRows = new OdbcCommand("SELECT * from " + tableName, conn))
            {
                OdbcDataAdapter adapter = new OdbcDataAdapter(readRows);
                adapter.Fill(dataTable);
            }
        }
    }
}
